import java.util.Scanner;
public class Menu_restoran {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        System.out.println("MENU MAKANAN : ");
        System.out.println("___________________________________________________________________");
        System.out.println("No Nama                     Harga");
        System.out.println("1. Nasi Goreng          Rp 25.000,00");
        System.out.println("2. Nasi Bakar           Rp 30.000,00");
        System.out.println("3. Nasi Padang          Rp 20.000,00");
        System.out.println("");

        int harga = 0;

        int h1 = 25000, h2 = 30000, h3 = 20000;
        String menu;

        for (String i = "Y"; i.equals("Y") || i.equals("y"); ) {
            System.out.println("___________________________________________________________________");

            System.out.print("Masukkan Nomor Pesanan : ");
            int inNomor = scan.nextInt();
            System.out.println("___________________________________________________________________");

            if (inNomor == 1) {
                menu = " Nasi Goreng";
                System.out.println("Pilihan anda nomor " + inNomor + menu);
                harga = harga + h1;
            } else if (inNomor == 2) {
                menu = " Nasi Bakar";
                System.out.println("Pilihan anda nomor " + inNomor + menu);
                harga = harga + h2;
            } else if (inNomor == 3) {
                menu = " Nasi Padang";
                System.out.println("Pilihan anda nomor " + inNomor + menu);
                harga = harga + h3;
            } else
            {
                System.out.println("Nomor yang dipilih tidak ada di menu.");
            }

            System.out.println("Apakah anda mau melanjutkan? Y/T");
            i = scan.next();

        }
        System.out.println("___________________________________________________________________");
        System.out.println("Total pembayaran sebesar " + harga + ".");
        System.out.println("Terimakasih atas kunjungannya.");
    }
}
